#! /bin/bash

# setup_init_update_env defines needed environment variables.

function search_parameters {
    #Search named parameters
    while [[ "$#" -gt 0 ]]
    do
    case $1 in
        --image)
            echo "The parameter --image is deprecated, you need to use the combination --environment and --version in the new branching model."
            exit 1
        ;;
        -h|--help)
            display_init_update_help
            exit 0
        ;;
        --crl2db)
            USE_CRL2DB="true"
        ;;
        --cors-origins)
            CORS_ORIGINS="$2"
        ;;
        --crl2db_password)
            CRL2DB_PASSWORD="$2"
        ;;
        --debug)
            DEBUG=2
        ;;
        --idhub_encryption)
            IDHUB_ENC_PWD="$2"
        ;;
        --kafka)
            KAFKA="$2"
        ;;
        --log-level)
            LOG_LEVEL="$2"
        ;;
        --mysql-password)
            MYSQL_PASSWORD="$2"
        ;;
        --mysql-root-password)
            MYSQL_ROOT_PASSWORD="$2"
        ;;
        --password)
            IDHUB_PASSWORD="$2"
        ;;
        --redis)
            REDIS-SERVER="$2"
        ;;
        --restore)
            RESTORE="true"
        ;;
        --tba-password)
            TBA_PASSWORD="$2"
        ;;
        --vasco)
            USE_VASCO="true"
        ;;
        --version)
            TB_VERSION="$2"
        ;;
        --vhost)
            VHOST="$2"
        ;;
    esac
    shift
    done
}

function make_sure_variables_have_value {
    # --- make sure environment has a value
    if [ -z "${ENVIRONMENT}" ]; then
        ENVIRONMENT=$(read_cfg "ENVIRONMENT" "production")
    fi

    # --- make sure crl2db has a value
    if [ -z "${USE_CRL2DB}" ]; then
        USE_CRL2DB=$(read_cfg "USE_CRL2DB" "false")
    fi
    
    # --- make sure crl2db password has a value
    if [ -z "${CRL2DB_PASSWORD}" ]; then
        TEMP_CRL2DB_PASSWORD=$(generate_password 32)
        CRL2DB_PASSWORD=$(read_cfg "CRL2DB_PASSWORD" "${TEMP_CRL2DB_PASSWORD}")
    fi
    
    # --- make sure idhub encryption password has a value
    if [ -z "${IDHUB_ENC_PWD}" ]; then
        TEMP_IDHUB_ENC_PWD=$(generate_password 32)
        IDHUB_ENC_PWD=$(read_cfg "IDHUB_ENC_PWD" "${TEMP_IDHUB_ENC_PWD}")
    fi
    
    # --- make sure kafka has a value
    if [ -z "${KAFKA}" ]; then
        KAFKA=$(read_cfg "KAFKA" ":@kafka:9092")
    fi

    # --- make sure log level has a value
    if [ -z "${LOG_LEVEL}" ]; then
        LOG_LEVEL=$(read_cfg "LOG_LEVEL" "INFO")
    fi

    # --- make sure mysql password has a value
    if [ -z "${MYSQL_PASSWORD}" ]; then
        TEMP_MYSQL_PASSWORD=$(generate_password 32)
        MYSQL_PASSWORD=$(read_cfg "MYSQL_PASSWORD" "${TEMP_MYSQL_PASSWORD}")
    fi
    
    # --- make sure mysql root password has a value
    if [ -z "${MYSQL_ROOT_PASSWORD}" ]; then
        TEMP_MYSQL_ROOT_PASSWORD=$(generate_password 32)
        MYSQL_ROOT_PASSWORD=$(read_cfg "MYSQL_ROOT_PASSWORD" "${TEMP_MYSQL_ROOT_PASSWORD}")
    fi
    
    # --- make sure idhub password has a value
    if [ -z "${IDHUB_PASSWORD}" ]; then
        TEMP_IDHUB_PASSWORD=$(generate_password 32)
        IDHUB_PASSWORD=$(read_cfg "IDHUB_PASSWORD" "${TEMP_IDHUB_PASSWORD}")
    fi

    # --- make sure redis server has a value
    if [ -z "${REDIS_SERVER}" ]; then
        REDIS_SERVER=$(read_cfg "REDIS_SERVER" "redis://redis:6379")
    fi

    # --- make sure tba password has a value
    if [ -z "${TBA_PASSWORD}" ]; then
        TEMP_TBA_PASSWORD=$(generate_password 32)
        TBA_PASSWORD=$(read_cfg "TBA_PASSWORD" "${TEMP_TBA_PASSWORD}")
    fi
    
    # --- make sure tb version has a value
    if [ -z "${TB_VERSION}" ]; then
        TB_VERSION=$(read_cfg "TB_VERSION" "10.4.5")
    fi

    # --- make sure tenant has a value
    TENANT=$(read_cfg "TENANT" "local")

    # --- make sure vasco has a value
    if [ -z "${USE_VASCO}" ]; then
        USE_VASCO=$(read_cfg "USE_VASCO" "false")
    fi

    # --- make sure vhost has a value
    if [ -z "${VHOST}" ]; then
        VHOST=$(read_cfg "VHOST" "")
    fi

    # --- check required variables
    if [ -z "${VHOST}" ]; then
        fatal "You need to specify a vhost to continue!"
    fi

    # --- make sure cors origins has a value
    if [ -z "${CORS_ORIGINS}" ]; then
        CORS_ORIGINS=$(read_cfg "CORS_ORIGINS" "${VHOST}")
    fi
}

function set_derived_variables {
    # --- Derived variables
    TB_VERSION_SHORT=$(echo ${TB_VERSION} | cut -d"." -f1).$(echo ${TB_VERSION} | cut -d"." -f2)
    TB_VERSION_X="${TB_VERSION_SHORT}.x"

    IMAGE_TAG="${TB_VERSION}"
    REPOSITORY="docker.trustbuilder.io/"
    SHELL="TB-${IMAGE_TAG}"
    UMAN_BRANCH_SEARCH="release/${TB_VERSION_SHORT}"

    KAFKA_SERVER=$( echo ${KAFKA} | cut -d@ -f2 )
    KAFKA_USER_PASS=$( echo ${KAFKA} | cut -d@ -f1 )
    KAFKA_USER=$( echo ${KAFKA_USER_PASS} | cut -d: -f1 )
    KAFKA_PASSWORD=$( echo ${KAFKA_USER_PASS} | cut -d: -f2 )

    GATEWAY_ID=${TENANT}_gateway
    GATEWAY_BOOTSTRAP=$(read_cfg "GATEWAY_BOOTSTRAP" "")
}

function write_to_env_file {
    # --- write install type to cfg
    echo "#this file is autogenerated by TrustBuilder and can be used to start other environments" > "${TB_DOCKER_BASE_FOLDER}/.env"
    
    write_cfg "COMPOSE_PROJECT_NAME" "tbconnect"
    write_cfg "CORS_ORIGINS" "${CORS_ORIGINS}"
    debug "CORS_ORIGINS=${CORS_ORIGINS}"
    write_cfg "USE_CRL2DB" "${USE_CRL2DB}"
    debug "USE_CRL2DB=${USE_CRL2DB}"
    write_cfg "CRL2DB_PASSWORD" "${CRL2DB_PASSWORD}"
    write_cfg "ENVIRONMENT" "${ENVIRONMENT}"
    debug "ENVIRONMENT=${ENVIRONMENT}"
    write_cfg "IDHUB_ENC_PWD" "${IDHUB_ENC_PWD}"
    write_cfg "KAFKA" "${KAFKA}"
    debug "KAFKA=${KAFKA}"
    write_cfg "LOG_LEVEL" "${LOG_LEVEL}"
    debug "LOG_LEVEL=${LOG_LEVEL}"
    write_cfg "MYSQL_PASSWORD" "${MYSQL_PASSWORD}"
    write_cfg "MYSQL_ROOT_PASSWORD" "${MYSQL_ROOT_PASSWORD}"
    write_cfg "IDHUB_PASSWORD" "${IDHUB_PASSWORD}"
    write_cfg "REDIS_SERVER" "${REDIS_SERVER}"
    debug "REDIS_SERVER=${REDIS_SERVER}"
    write_cfg "TBA_PASSWORD" "${TBA_PASSWORD}"
    write_cfg "USE_VASCO" "${USE_VASCO}"
    debug "USE_VASCO=${USE_VASCO}"
    write_cfg "TB_VERSION" "${TB_VERSION}"
    debug "TB_VERSION=${TB_VERSION}"
    write_cfg "TENANT" "${TENANT}"
    debug "TENANT=${TENANT}"
    write_cfg "VHOST" "${VHOST}"
    debug "VHOST=${VHOST}"
    echo "" >> "${TB_DOCKER_BASE_FOLDER}/.env"
    
    write_cfg "KAFKA_SERVER" "${KAFKA_SERVER}"
    debug "KAFKA_SERVER=${KAFKA_SERVER}"
    write_cfg "KAFKA_USER" "${KAFKA_USER}"
    debug "KAFKA_USER=${KAFKA_USER}"
    write_cfg "KAFKA_PASSWORD" "${KAFKA_PASSWORD}"
    debug "KAFKA_PASSWORD=${KAFKA_PASSWORD}"
    write_cfg "GATEWAY_ID" "${GATEWAY_ID}"
    debug "GATEWAY_ID=${GATEWAY_ID}"
    write_cfg "GATEWAY_BOOTSTRAP" "${GATEWAY_BOOTSTRAP}" 
}

function setup_init_update_env {
    RESTORE="false"
    unset CONFIRMED

    # --- bail if we are root ---..
    if [ $(id -u) -eq 0 ]; then
        fatal "You cannot be root to perform this install"
    fi

    # --- Make sure the config file exists
    if [ ! -f "${TB_DOCKER_BASE_FOLDER}/.env" ]; then
        echo "#this file is autogenerated by TrustBuilder and can be used to start other environments" > "${TB_DOCKER_BASE_FOLDER}/.env"
    fi

    # --- Make sure the workflow folder exists
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/data/IDHUB_CUSTOM_HOME" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/data/IDHUB_CUSTOM_HOME"
    fi

    # --- Make sure the custom context folder exists
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/data/idhub.d" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/data/idhub.d"
    fi

    # --- Make sure the custom libs folder exists
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/data/tomcat-libs" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/data/tomcat-libs"
    fi

    # --- Make sure the backup folder exists
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/backup" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/backup"
    fi

    search_parameters "$@"
    make_sure_variables_have_value
    set_derived_variables
    write_to_env_file

    echo "" >> "${TB_DOCKER_BASE_FOLDER}/.env"

    set_bash_trustbuilder ${SHELL}
    DOCKER_COMPOSE_LOG="${TB_DOCKER_BASE_FOLDER}/data/tb-deploy.log"
    touch ${DOCKER_COMPOSE_LOG}
    prepare_kafka_settings
}

function prepare_crl2db {
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/data/crl2db" ];
    then
        mkdir -p ${TB_DOCKER_BASE_FOLDER}/data/crl2db
        info "Please copy the trustbuilder.properties file into ${TB_DOCKER_BASE_FOLDER}/data/crl2db folder and restart the crl2db service!"
        sleep 5
    fi
    sleep 30
    docker-compose exec mysql mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS CRL2DB;" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE USER 'crl2db'@'%' IDENTIFIED BY '${CRL2DB_PASSWORD}';" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON *.* TO 'crl2db'@'%';" &> ${DOCKER_COMPOSE_LOG}

    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE TABLE CERT_AUTHS (CA_ID VARCHAR(20) NOT NULL, AKI VARCHAR(60), ISSUER VARCHAR(255), DOWNLOAD_URI VARCHAR(50), CONSTRAINT CERT_AUTHS_PK PRIMARY KEY (CA_ID));" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE TABLE CRLS (ID INT AUTO_INCREMENT, CA_ID VARCHAR(20), FULL_CRL VARCHAR(50), DELTA_CRL VARCHAR(50), CONSTRAINT CRLS_PK PRIMARY KEY (ID), CONSTRAINT FK_CERT_AUTH FOREIGN KEY (CA_ID) REFERENCES CERT_AUTHS (CA_ID) ON DELETE CASCADE, CONSTRAINT CRLS_CA UNIQUE (CA_ID, FULL_CRL));" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE INDEX IDX_CRLSFULL ON CRLS (CA_ID, FULL_CRL);" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE INDEX IDX_CRLSDELTA ON CRLS (CA_ID, DELTA_CRL);" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE TABLE REVOKED (SERIAL_NUMBER VARCHAR(50) NOT NULL, REVOCATION_DATE TIMESTAMP, INVALIDITY_DATE TIMESTAMP, REASON_CODE SMALLINT, REASON VARCHAR(50), CRL_ID INTEGER NOT NULL, CRLNUMBER BIGINT, CONSTRAINT REVOKED_PER_CRL UNIQUE (SERIAL_NUMBER, CRL_ID, REASON_CODE), CONSTRAINT FK_CRLS_REV FOREIGN KEY (CRL_ID) REFERENCES CRLS (ID) ON DELETE CASCADE);" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE INDEX IDX_REV_ID_REV ON REVOKED (CRL_ID, CRLNUMBER);" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE INDEX IDX_REV_SERIAL ON REVOKED (SERIAL_NUMBER);" &> ${DOCKER_COMPOSE_LOG}
    docker-compose exec mysql mysql CRL2DB -ucrl2db -p${CRL2DB_PASSWORD} -e "CREATE TABLE STATUS_INFO (CRL_ID INTEGER NOT NULL, PUB_DATE TIMESTAMP, NEXT_PUB_DATE TIMESTAMP, FULL_PUB_DATE TIMESTAMP, LAST_UPDATE TIMESTAMP, CRLNUMBER BIGINT, CONSTRAINT STATUS_INFO_PK PRIMARY KEY (CRL_ID), CONSTRAINT FK_CRLS_SI FOREIGN KEY (CRL_ID) REFERENCES CRLS (ID) ON DELETE CASCADE);" &> ${DOCKER_COMPOSE_LOG}
    
    echo "<Resource name=\"jdbc/crl2db\" url=\"jdbc:mysql/CRL2DB\"" > ${TB_DOCKER_BASE_FOLDER}/data/idhub.d/crl2db.xml
    echo "driverClassName=\"com.mysql.cj.jdbc.Driver\" username=\"crl2db\"" >> ${TB_DOCKER_BASE_FOLDER}/data/idhub.d/crl2db.xml
    echo "password=\"${CRL2DB_PASSWORD}\" auth=\"Container\" type=\"javax.sql.DataSource\"" >> ${TB_DOCKER_BASE_FOLDER}/data/idhub.d/crl2db.xml
    echo "maxTotal=\"151\" maxIdle=\"10\" maxWaitMillis=\"10000\" connectionProperties=\"failOverReadOnly=false;autoReconnect=true;validConnectionTimeout=30\" />" >> ${TB_DOCKER_BASE_FOLDER}/data/idhub.d/crl2db.xml

    init_docker_override
    echo "  crl2db:" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "    image: \${CRL2DB_IMAGE}" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "    environment:" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "      TB_CRL2DB_DB_PASSWORD: ${CRL2DB_PASSWORD}" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "      TB_CRL2DB_DB_URI: mysql://mysql:3306/CRL2DB?serverTimezone=UTC" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "    depends_on:" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "      - mysql" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "    volumes:" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    echo "      - ./crl2db:/TB_HOME" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
}

function prepare_kafka_settings {
    echo "" > ${TB_DOCKER_BASE_FOLDER}/data/idhub.d/kafka.xml
}

function init_docker_override {
    if [ ! -f "${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml" ]; then
        echo "---" > ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
        echo "version: \"3.8\"" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
        echo "services:" >> ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
    fi
}

function init_gateway {
    wait_for_orchestrator

    docker-compose exec orchestrator init_gateway.sh ${GATEWAY_ID}
    GATEWAY_BOOTSTRAP=$(docker-compose exec orchestrator cat /bootstrap/${GATEWAY_ID}.token)
    write_cfg "GATEWAY_BOOTSTRAP" "${GATEWAY_BOOTSTRAP}"
    docker-compose up -d gateway
}

function wait_for_orchestrator {
    info "Waiting for the orchestrator to start..."
    orchestrator_status="000"
    i=1
    while [ "${orchestrator_status}" != "200" ];
    do
        if [ "$i" == 18 ];
        then
            GIT_COMMIT=$(read_docker_env orchestrator GIT_COMMIT)
            info "Exporting logs to logs/${GIT_COMMIT}.zip"
            export_logs
            fatal "startup is taking to long, please contact your Administrator"
        fi
        orchestrator_status=$(docker-compose exec orchestrator curl --write-out '%{http_code}' --silent --output /dev/null localhost:8080/idhub/oidc/v1/jwks.json || echo "")
        sleep 5
        i=$((i+1))
    done
}

function display_init_update_help {
    display_help_header
    echo "tb-deploy init|update [[--options]]"
    echo "  --cors-origins          Set the CORS for all modules. [\$CORS_ORIGINS]"
    echo "                            Default is \"\""
    echo "  --crl2db                Indicates if CRL2DB needs to be started on the system. [\$USE_CRL2DB]"
    echo "                            Default is \"false\""
    echo "  --crl2db-password       Token that will be used for crl2db encryption. [\$CRL2DB_PASSWORD]"
    echo "                            Default is 32 character generated"
    echo "  --help                  Displays this help menu"
    echo "  --idhub-encryption      Token that will be used for IDHUB encryption. [\$IDHUB_ENC_PWD]"
    echo "                            Default is 32 character generated"
    echo "  --kafka                 The uri that will be used to connect to kafka. [\$KAFKA]"
    echo "                            Default is \":@kafka:9092\""
    echo "  --log-level             The log level to use for the docker images. [\$LOG_LEVEL]"
    echo "                            Default is \"INFO\""
    echo "  --mysql-password        Password that will be set for mysql IDHUB user. [\$MYSQL_PASSWORD]"
    echo "                            Default is 32 character generated"
    echo "  --mysql-root-password   Password that will be set for mysql root user. [\$MYSQL_ROOT_PASSWORD]"
    echo "                            Default is 32 character generated"
    echo "  --password              Password that will be set for the admin portal. [\$IDHUB_PASSWORD]"
    echo "                            Default is 32 character generated"
    echo "  --redis                 The redis server to use for this setup. [\$REDIS-SERVER]"
    echo "                            Default is \"redis://redis:6379\""
    echo "  --restore               Use the restore files located in the folder backup."
    echo "  --tba-password          Password that will be set for the tba portal. [\$TBA_PASSWORD]"
    echo "                            Default is 32 character generated"
    echo "  --tenant                Set the tenant name of the current installation. [\$TENANT]"
    echo "                            Default is \"\""
    echo "  --vasco                 Activate vasco."
    echo "  --version               To be used in combination with --image. [\$TB_VERSION]"
    echo "                            Default is \"10.4.5\""
    echo "  --vhost                 Set the default vhost that will be used for the enviornment. [\$VHOST]"
    echo "                            Needs to contain http:// or https://"
    echo ""
    echo "Passwords or Tokens are autogenerated if not set"
    echo ""
    echo "Examples:"
    echo "tb-deploy init --password theVerySecretPassword --vhost https://mytenant.trustbuilder.io --tenant mytenant"
}