#! /bin/bash

function setup_clean_env {
    unset CONFIRMED

    while [[ "$#" -gt 0 ]]
    do
    case $1 in
        -h|--help)
            display_clean_help
            exit 0
        ;;
        --all-data)
            ALL_CONFIRMED="y"
            BACKUP_CONFIRMED="y"
            IDHUBD_CONFIRMED="y"
            LIBS_CONFIRMED="y"
        ;;
        --backup)
            BACKUP_CONFIRMED="y"
        ;;
        --idhubd)
            IDHUBD_CONFIRMED="y"
        ;;
        --libs)
            LIBS_CONFIRMED="y"
        ;;
        -y|--yes)
            CONFIRMED="y"
        ;;
    esac
    shift
    done

    GIT_COMMIT=$(read_docker_env orchestrator GIT_COMMIT)
    DOCKER_COMPOSE_LOG="${TB_DOCKER_BASE_FOLDER}/data/tb-deploy.log"
}

function clean {
    if [ -z "${CONFIRMED}" ];
    then
        read -p "Do you want to clean the installation (y/N)? " CONFIRMED
    fi

    if [ "${CONFIRMED}" == "y" ];
    then
        # exit on error
        set -e 

        info "Shutdown containers"
        docker-compose down --rmi all --volumes --remove-orphans &> ${DOCKER_COMPOSE_LOG}

        info "Remove folders"
        rm -rf ${TB_DOCKER_BASE_FOLDER}/docker-compose.override.yml
        rm -rf ${TB_DOCKER_BASE_FOLDER}/.env
        rm -rf ${TB_DOCKER_BASE_FOLDER}/service-catalogue
        
        info "Remove folders with sudo rights"
        sudo rm -rf ${TB_DOCKER_BASE_FOLDER}/data/IDHUB_CUSTOM_HOME

        info "Set system status to \"clean\""

        if [ "${BACKUP_CONFIRMED}" == "y" ]; then
            info "Remove backup folder"
            sudo rm -rf "${TB_DOCKER_BASE_FOLDER}/backup"
        fi
        if [ "${IDHUBD_CONFIRMED}" == "y" ]; then
            info "Remove idhub.d folder"
            sudo rm -rf "${TB_DOCKER_BASE_FOLDER}/data/idhub.d"
        fi
        if [ "${LIBS_CONFIRMED}" == "y" ]; then
            info "Remove tomcat-libs folder"
            sudo rm -rf "${TB_DOCKER_BASE_FOLDER}/data/tomcat-libs"
        fi
        set_bash_trustbuilder "clean"
    fi
}

function display_clean_help {
    display_help_header
    echo "tb-deploy clean [[--options]]"
    echo "  --help                  Displays this help menu"
    echo "  --all-data              Clean all data"
    echo "  --backup                Include backup in cleanup"
    echo "  --idhubd               Include idhub.d folder with custom contexts"
    echo "  --libs                  Include tomcat-libs folder"
    echo "  --yes|-y                Silent confirm the clean action."
    echo ""
}