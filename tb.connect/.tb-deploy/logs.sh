#! /bin/bash

function setup_log_env {
    CLEAN_LOGS="false"
    EXPORT_LOGS="false"
    unset FOLLOW
    unset CONFIRMED

    while [[ "$#" -gt 0 ]]
    do
    case $1 in
        -h|--help)
            display_log_help
            exit 0
        ;;
        --clean)
            CLEAN_LOGS="true"
        ;;
        --export)
            EXPORT_LOGS="true"
        ;;
        -f|--follow)
            FOLLOW="$2"
        ;;
        -y|--yes)
            CONFIRMED="y"
        ;;
    esac
    shift
    done
    
    GIT_COMMIT=$(read_docker_env orchestrator GIT_COMMIT)
    DOCKER_COMPOSE_LOG="${TB_DOCKER_BASE_FOLDER}/data/tb-deploy.log"
}

function clean_logs {
     if [ -z "${CONFIRMED}" ];
    then
        read -p "Do you want to clean the logs (y/N)? " CONFIRMED
    fi

    if [ "${CONFIRMED}" == "y" ];
    then
        sudo truncate -s 0 $(docker inspect --format='{{.LogPath}}' docker_int_gateway_1)
        sudo truncate -s 0 $(docker inspect --format='{{.LogPath}}' docker_int_orchestrator_1)

        rm -rf "${TB_DOCKER_BASE_FOLDER}/logs"
    fi
}

function export_logs {
    # --- Make sure the log folder exists
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}"
    fi
    if [ ! -d "${TB_DOCKER_BASE_FOLDER}/logs" ]; then
        mkdir -p "${TB_DOCKER_BASE_FOLDER}/logs"
    fi
    services=$(docker-compose  config --services)
    for service in ${services}; do
        if [ "${service}" != "backend-sp" ] && [ "${service}" != "mysql" ]; then
            info "Exporting ${service}"
            docker-compose logs --no-log-prefix ${service} > ${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}/${service}.log
        fi
    done

    cp .env ${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}/environment

    zip -r ${TB_DOCKER_BASE_FOLDER}/logs/${GIT_COMMIT}.zip "${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}"
    rm -rf "${TB_DOCKER_BASE_FOLDER}/${GIT_COMMIT}"
}

function display_log_help {
    display_help_header
    echo "tb-deploy logs [[--options]]"
    echo "  --clean                 Clean all the known log files for docker"
    echo "  --export                Export all the log files to a zip file"
    echo "  --folow|-f              Follow the log file of the mentioned module"
    echo "  --help                  Displays this help menu"
    echo "  --yes|-y                Silent confirm the clean action."
    echo ""
}