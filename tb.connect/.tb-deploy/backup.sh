#! /bin/bash

function setup_backup_env {
    unset CONFIRMED

    while [[ "$#" -gt 0 ]]
    do
    case $1 in
        -h|--help)
            display_backup_help
            exit 0
        ;;
    esac
    shift
    done

    DOCKER_COMPOSE_LOG="${TB_DOCKER_BASE_FOLDER}/data/tb-deploy.log"
    MYSQL_PASSWORD=$(read_cfg "MYSQL_PASSWORD" "")
}

function backup {
    info "Create backup from database"
    docker-compose exec mysql mysqldump -uidhub -p${MYSQL_PASSWORD} IDHUB > ${TB_DOCKER_BASE_FOLDER}/backup/idhub.sql
    info "Create backup from gateway"
    docker-compose exec gateway cp -f client/gateway.jks /backup/gateway.jks
    docker-compose exec gateway cp -f client/gateway.properties /backup/gateway.properties
    info "Create backup from data"
    sudo cp -r ${TB_DOCKER_BASE_FOLDER}/data/* ${TB_DOCKER_BASE_FOLDER}/backup/data/
}

function restore {
    info "Restoring database"
    docker-compose exec mysql mysql -uidhub -p${MYSQL_PASSWORD} -e 'DROP DATABASE IDHUB;'
    docker-compose exec mysql mysql -uidhub -p${MYSQL_PASSWORD} -e 'CREATE DATABASE IDHUB;'
    docker-compose exec mysql mysql -uidhub -p${MYSQL_PASSWORD} IDHUB < ${TB_DOCKER_BASE_FOLDER}/backup/idhub.sql

    info "Restoring gateway"
    docker-compose exec gateway cp -f /backup/gateway.jks client/gateway.jks
    docker-compose exec gateway cp -f /backup/gateway.properties client/gateway.properties

    info "Restoring data"
    sudo cp -r ${TB_DOCKER_BASE_FOLDER}/backup/data/* ${TB_DOCKER_BASE_FOLDER}/data/

    info "Restarting orchestrator"
    docker-compose restart orchestrator &> ${DOCKER_COMPOSE_LOG}
    wait_for_orchestrator
    info "Restarting gateway"
    docker-compose restart gateway &> ${DOCKER_COMPOSE_LOG}
}

function display_backup_help {
    display_help_header
    echo "tb-deploy backup"
    echo "  Take a backup of the current environment. Current behaviour is overwriting previous backup."
}