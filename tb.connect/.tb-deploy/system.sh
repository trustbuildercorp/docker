#! /bin/bash

function prepare_system {
    while [[ "$#" -gt 0 ]]
    do
    case $1 in
        -h|--help)
            display_setup_help
            exit 0
        ;;
        --uman-path)
            UMAN_PATH=$2
        ;;
    esac
    shift
    done

    if [ -z "${UMAN_PATH}" ]; then
        fatal "Uman path is required!"
    fi

    if grep -Fxq "source ~/.bash_trustbuilder" ~/.bashrc
    then
        debug "System already initialized"
    else
        echo "source ~/.bash_trustbuilder" >> ~/.bashrc
        echo "alias tb-deploy=\"${TB_DOCKER_BASE_FOLDER}/tb-deploy\"" >> ~/.bashrc
        touch ~/.bash_trustbuilder
    fi
}

function set_bash_trustbuilder {
    cat <<EOF > ~/.bash_trustbuilder
PS1="(${1}) \${PS1:-}"
export PS1
export TB_DOCKER_BASE_FOLDER=${TB_DOCKER_BASE_FOLDER}
export UMAN_PATH=${UMAN_PATH}
hash -r
EOF
}

function display_setup_help {
    display_help_header
    echo "tb-deploy logs [[--options]]"
    echo "  --help                  Displays this help menu"
    echo "  --uman-path             Set the path to the uman folder"
    echo ""
}