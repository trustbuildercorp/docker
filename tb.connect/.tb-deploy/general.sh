#! /bin/bash

# info logs the given argument at info log level.
function info {
    echo "[INFO] " "$@"
}

# warn logs the given argument at warn log level.
function warn {
    echo "[WARN] " "$@" >&2
}

# fatal logs the given argument at fatal log level.
function fatal {
    echo "[ERROR] " "$@" >&2
    if [ -n "${SUFFIX}" ]; then
        echo "[ALT] trustbuilder --help for more information or go to 'https://support.trustbuilder.com'" >&2
    fi
    exit 1
}

function debug {
    if [ "${DEBUG}" = 2 ]; then
        echo "[DEBUG] " "$@"
    fi
}

function ok {
    echo "[V] " "$@"
}

function nok {
    echo "[ ] " "$@"
}

function ProgressBar {
    # Process data
        let _progress=(${1}*100/${2}*100)/100
        let _done=(${_progress}*4)/10
        let _left=40-$_done
    # Build progressbar string lengths
        _fill=$(printf "%${_done}s")
        _empty=$(printf "%${_left}s")

    # 1.2 Build progressbar strings and print the ProgressBar line
    # 1.2.1 Output example:                           
    # 1.2.1.1 Progress : [########################################] 100%
    printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"
}

function generate_password {
    echo $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-16} | head -n 1)
}

function read_cfg {
    thekey="${1}"
    default="${2}"
    result=$(sed -rn "s/^${thekey}=([^\n]+)$/\1/p" "${TB_DOCKER_BASE_FOLDER}/.env")
    echo ${result:-$default}
}

function write_cfg {
    thekey="${1}"
    newvalue="${2}"

    if ! grep -R "^[#]*\s*${thekey}=.*" "${TB_DOCKER_BASE_FOLDER}/.env" > /dev/null; then
        echo "${thekey}=${newvalue}" >> "${TB_DOCKER_BASE_FOLDER}/.env"
    else
        sed -ir "s#^[#]*\s*${thekey}=.*#$thekey=$newvalue#" "${TB_DOCKER_BASE_FOLDER}/.env"
    fi

    return 0
}

function read_docker_env {
    what="${1}"
    thekey="${2}"
    result=$(docker-compose exec ${what} env | sed -rn "s/^${thekey}=([^\n]+)$/\1/p")
    echo ${result}
}